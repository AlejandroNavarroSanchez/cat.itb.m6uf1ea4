import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class EA4 {
    private static final File employeeFile = new File("FitxerEmpleats.dat");
    private static final File departmentFile = new File("FitxerDepartaments.dat");
    private static final String BOLD = "\033[1m"; // Text en negreta
    private static final String RESET = "\033[0m"; // Reset format

    // MAIN
    public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);

        //Variables
        int id;
        String newEmployee;
        String newLocation;

        showMenu();
        int n = lector.nextInt();
        boolean exit = false;
        while (!exit) {
            switch (n) {
                case 1:
                    employeeFileTemplate();
                    break;
                case 2:
                    departmentFileTemplate();
                    break;
                case 3:
                    readEmployees();
                    break;
                case 4:
                    readDepartments();
                    break;
                case 5:
                    addFiveEmployees();
                    break;
                case 6:
                    addFiveDepartments();
                    break;
                case 0:
                    exit = true;
                    return;
                default:
                    System.out.println("Opció invàlida. Torna a introduir-ne una altra:");
                    break;
            }
            promptEnterKey();
            showMenu();
            n = lector.nextInt();
        }
    }

    // Obliga a presionar ENTER para avanzar.
    private static void promptEnterKey() throws InterruptedException {
        System.out.println("\nPrem \"ENTER\" per a continuar...");

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    //MENU
    private static void showMenu() {
        System.out.println("\n\n\n\n\n");
        //Exericics
        System.out.println("FITXERS BINARIS:");
        System.out.println();
        //1
        System.out.println("1.- Crea i afegeix 5 registres a \"FitxerEmpleats.dat\".");
        //2
        System.out.println("2.- Crea i afegeix 5 registres a \"FitxerDepartaments.dat\"");
        //3
        System.out.println("3.- Llegeix el fitxer \"FitxerEmpleats.dat\".");
        //4
        System.out.println("4.- Llegeix el fitxer \"FitxerDepartaments.dat\".");
        //5
        System.out.println("5.- Afegeix 5 registres a \"FitxerEmpleats.dat\".");
        //6
        System.out.println("6.- Afegeix 5 registres a \"FitxerDepartaments.dat\"");
        System.out.println();
        //Exit
        System.out.println("0.- SORTIR");
    }

    /*
     2.- Crea els mètodes per escriure 5 objectes Empleat en un fitxer anomenat "FitxerEmpleats" i per escriure 5
     objectes Departament en un fitxer anomenat "FitxerDepartaments".
     */
    /**
     * Crea l'arxiu FitxerEmpleats.dat amb 5 registres base.
     * @throws IOException
     */
    private static void employeeFileTemplate() throws IOException {
        // En cas de que no existeixi el fitxer
        if (!employeeFile.exists()) {
            // Informació base del fitxer
            ArrayList<Empleat> employees = new ArrayList<>(Arrays.asList(
                    new Empleat(1, "González", 1, 1500.0),
                    new Empleat(2, "Álvarez", 2, 1300.0),
                    new Empleat(3, "López", 3, 1250.0),
                    new Empleat(4, "Navarro", 4, 1100.5),
                    new Empleat(5, "Sánchez", 5, 900.8)
            ));

            ObjectOutputStream dataOS = new ObjectOutputStream(new FileOutputStream(employeeFile));

            for (int i = 0; i < employees.size(); i++) {
                dataOS.writeObject(employees.get(i));
            }
            dataOS.close();
            System.out.println("Fitxer " + BOLD + employeeFile.getName() + RESET + " creat correctament.");
        } else
            System.out.println("El fitxer " + BOLD + employeeFile.getName() + RESET + " ja existeix.");

    }

    /**
     * Crea l'arxiu FitxerDepartaments.dat amb 5 registres base.
     * @throws IOException
     */
    private static void departmentFileTemplate() throws IOException {
        // En cas de que no existeixi el fitxer
        if (!departmentFile.exists()) {
            // Informació base del fitxer
            ArrayList<Departament> departments = new ArrayList<>(Arrays.asList(
                    new Departament(1, "Comercial", "Masquefa"),
                    new Departament(2, "Marketing", "Barcelona"),
                    new Departament(3, "RRHH", "Masnou"),
                    new Departament(4, "Directiva", "El Prat de Llobregat"),
                    new Departament(5, "Comunicación", "Tarragona")
            ));

            ObjectOutputStream dataOS = new ObjectOutputStream(new FileOutputStream(departmentFile));

            for (int i = 0; i < departments.size(); i++) {
                dataOS.writeObject(departments.get(i));
            }
            dataOS.close();
            System.out.println("Fitxer " + BOLD + departmentFile.getName() + RESET + " creat correctament.");
        } else
            System.out.println("El fitxer " + BOLD + departmentFile.getName() + RESET + " ja existeix.");

    }

    /*
     3.- Crea els mètodes per llegir els fitxers "FitxerEmpleats" i "FitxerDepartaments".
     */

    /**
     * Llegeix els empleats del fitxer FitxerEmpleats.dat
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private static void readEmployees() throws IOException, ClassNotFoundException {
        Empleat e;
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(employeeFile));
        try {
            while (true) {
                e = (Empleat) dataIS.readObject();
                System.out.println(e);
            }
        } catch (EOFException eo) {
            System.out.println("\nFIN DE LECTURA.");
        }
        dataIS.close();
    }

    /**
     * Llegeis els departamnents del fitxer FitxerDepartaments.dat
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private static void readDepartments() throws IOException, ClassNotFoundException {
        Departament d;
        ObjectInputStream dataIS = new ObjectInputStream(new FileInputStream(departmentFile));
        try {
            while (true) {
                d = (Departament) dataIS.readObject();
                System.out.println(d);
            }
        } catch (EOFException eo) {
            System.out.println("\nFIN DE LECTURA.");
        }
        dataIS.close();
    }

    /*
     4.- Afegeix 5 objectes Empleat al fitxer "FitxerEmpleats" i 5 objectes Departament al fitxer
     "FitxerDepartaments". i comprova que es poden llegir correctament amb els mètodes creats a l'exercici 3.
     */

    /**
     * Afegeix 5 empleats a FitxerEmpleats.dat
     * @throws IOException
     */
    private static void addFiveEmployees() throws IOException {
        ArrayList<Empleat> employees = new ArrayList<>(Arrays.asList(
                new Empleat(6, "Fernández", 1, 1500.0),
                new Empleat(7, "Gálvez", 2, 1300.0),
                new Empleat(8, "García", 3, 1250.0),
                new Empleat(9, "Navarro", 4, 1100.5),
                new Empleat(10, "Sánchez", 5, 900.8)
        ));

        MyObjectOutputStream dataOS = new MyObjectOutputStream(new FileOutputStream(employeeFile, true));

        for (int i = 0; i < employees.size(); i++) {
            dataOS.writeObject(employees.get(i));
        }
        dataOS.close();
        System.out.println("S'han afegit les dades correctament al fitxer" + BOLD + employeeFile.getName() + RESET);
    }

    /**
     * Afegeix 5 departaments a FitxerDepartaments.dat
     * @throws IOException
     */
    private static void addFiveDepartments() throws IOException {
        ArrayList<Departament> departments = new ArrayList<>(Arrays.asList(
                new Departament(6, "Contabilidad", "Masquefa"),
                new Departament(7, "Ventas", "Barcelona"),
                new Departament(8, "Telecomunicaciones", "Masnou"),
                new Departament(9, "Informatica", "El Prat de Llobregat"),
                new Departament(10, "Estadistica", "Tarragona")
        ));

        MyObjectOutputStream dataOS = new MyObjectOutputStream(new FileOutputStream(departmentFile, true));

        for (int i = 0; i < departments.size(); i++) {
            dataOS.writeObject(departments.get(i));
        }
        dataOS.close();
        System.out.println("S'han afegit les dades correctament al fitxer" + BOLD + departmentFile.getName() + RESET);
    }
}
